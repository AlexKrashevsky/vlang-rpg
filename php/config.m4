dnl $Id$
dnl config.m4 for extension ile

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

PHP_ARG_WITH(ile, for ile support,
[  --with-ile=[DIR]     DIR is the location of includes ])

dnl PHP_ARG_ENABLE(ile, whether to enable ile support,
dnl Make sure that the comment is aligned:
dnl [  --enable-ile           Enable ile support])
if test "$PHP_ile" != "no"; then
  dnl # checking php 32/64 bit php
  AC_MSG_CHECKING(PHP)
  if test `php -r 'echo PHP_INT_SIZE;'` = 8; then
    machine_bits=64
    libDir=lib64
    AC_MSG_RESULT(Detected 64-bit PHP)
  else
    machine_bits=32
    libDir=lib32
    AC_MSG_RESULT(Detected 32-bit PHP)
  fi
  AC_MSG_CHECKING(IBM_PASE_HOME location)
  if test $IBM_PASE_HOME ; then
    SEARCH_PATH=$IBM_PASE_HOME
    AC_MSG_RESULT($IBM_PASE_HOME)
  else
    AC_MSG_RESULT(not found)
  fi
  dnl # --with-ile -> check with-path  	 
  SEARCH_PATH="/usr/lib /usr/include $PHP_ILE_LIB $SEARCH_PATH"

  AC_MSG_CHECKING(Looking for libraries)
  for i in $SEARCH_PATH ; do
    AC_MSG_CHECKING([     in $i])
    if test -r $i/libc.a ; then
      LIB_DIR="$i/"
      AC_MSG_RESULT(found)
      break
    else
      AC_MSG_RESULT()
    fi
    AC_MSG_CHECKING([     in $i/$libDir])
    if test -r $i/$libDir/libc.a ; then
      LIB_DIR="$i/$libDir/"
      AC_MSG_RESULT(found)
      break
    else
      AC_MSG_RESULT()
    fi
    AC_MSG_CHECKING([     in $i/lib])
    if test -r $i/lib/libc.a ; then
      LIB_DIR="$i/lib/"
      AC_MSG_RESULT(found)
      break
    else
      AC_MSG_RESULT()
    fi
  done

  if test -z "$LIB_DIR"; then
    AC_MSG_RESULT([not found])
    if test $IBM_PASE_HOME ; then
      AC_MSG_ERROR([Cannot find libraries. Check if you have set the IBM_PASE_HOME environment variable's value correctly])
    else
       AC_MSG_ERROR([Environment variable IBM_PASE_HOME is not set module install])
    fi
  fi

  AC_MSG_CHECKING([for PASE include files in default path])
  for i in $SEARCH_PATH ; do
    AC_MSG_CHECKING([in $i])
    dnl this is for V8.1 and previous
    if test -r "$i/as400_protos.h" ; then
      ile_DIR=$i
      AC_MSG_RESULT(found in $i)
      break
    fi
  done

  if test -z "$ile_DIR"; then
    AC_MSG_RESULT([not found])
    AC_MSG_ERROR([Please reinstall the PASE distribution])
  fi

  dnl # --with-ile -> add include path
  PHP_ADD_INCLUDE($ile_DIR/include)

  dnl # --with-ile -> check for lib and symbol presence
  LIBNAME=libc
  PHP_NEW_EXTENSION(ile, php_ile.c, $ext_shared)
  LIBSYMBOL=__ILELOAD

dnl #  PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
dnl #  [
    PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $LIB_DIR , ile_SHARED_LIBADD)
    AC_DEFINE(HAVE_PASELIB,1,[ ])
dnl #  ],[
dnl #    AC_MSG_ERROR([wrong PASE lib version or lib not found])
dnl #  ],[
dnl #    -L$ile_DIR/lib -lm -ldl
dnl #  ])

  PHP_SUBST(ile_SHARED_LIBADD)
fi

