# README #

VLANG is a RPG SRVPGM (VLANG/VPHP), providing in-memory calling of RPG<memory>PHP. 
Everything is controlled by RPG, thereby, PHP is essentially a slave language to RPG.

Note: VLANG is RPG-centric, aka, RPG starts PHP as a slave language. Therefore, you can not use this extension starting from PHP (or php web, etc.).

### How do I get set up? ###

### Download zip file (Downloads left) ###

### ftp ###
```
1) rpg to ... /usr/local/zendsvr6/share/vlang/rpg
2) php to ... /usr/local/zendsvr6/share/vlang/php
```

### compile (rpg) ###
```
> cd  /usr/local/zendsvr6/share/vlang/rpg
> ./makevphp.sh
> ./maketest.sh
```

### compile (php) ###
```
> cd  /usr/local/zendsvr6/share/vlang/php
> ./zzallzs6.sh
> ./zzmakezs6.sh
scripts require xlc compiler and zend server 8.x with php 5.6
```
Alternative pre-compiled pecl

* http://yips.idevcloud.com/wiki/index.php/RPG/RPGCallMe


### example run (see test0001.rpgle in rpg source) ###
```
bash-4.3$ cd  /usr/local/zendsvr6/share/vlang/php
bash-4.3$ ./zzallzs6.sh
-- or --
pre-compilied pecl ile.so, follow alternative steps (yips above) 

bash-4.3$ cd  /usr/local/zendsvr6/share/vlang/rpg
bash-4.3$ ./makevphp.sh
bash-4.3$ ./maketest.sh 
CPF2111: Library VLANG already exists.
====================================
==> VLANG/test0001 ...
==> VLANG/test0001 -- 00 highest severity
====================================
CRTPGM PGM(VLANG/test0001) MODULE(VLANG/test0001) BNDSRVPGM(VLANG/vphp)
CPC5D07: Program TEST0001 created in library VLANG.
bash-4.3$ system 'call vlang/test0001'
before ILE call ... hi from php,5.6,zend server,8,11.11,22.22
after ILE call ... hi from ILE RPG free.,4.1,VLANG rver,1,234.5678,321.21
```

### debug ###
* RPG can be debugged using standard ILE debuggers (many).
* PHP c code can be debugged by putting sleep in the PHP code, then attaching a PASE debugger (dbx -d 100 -a pid).
* PHP scripts, i dunno, never used a PHP debugger (see Alan, maybe he knows)

To help with early problems, added php.log output chat.php and helloile.php (Luca).
```
bash-4.3$ pwd
/usr/local/zendsvr6/share/vlang/rpg
bash-4.3$ echo start >  /usr/local/zendsvr6/var/log/php.log 
bash-4.3$ cat /usr/local/zendsvr6/var/log/php.log 
start
bash-4.3$ system 'call vlang/test0001'
before ILE call ... hi from php,5.6,zend server,8,11.11,22.22
after ILE call ... hi from ILE RPG free.,4.1,VLANG rver,1,234.5678,321.21
bash-4.3$ cat /usr/local/zendsvr6/var/log/php.log 
start
[17-Feb-2016 16:08:15 America/Atikokan] chat.php we are about to start ...
[17-Feb-2016 16:08:15 America/Atikokan] chat.php we are returning to ile ...
[17-Feb-2016 16:08:15 America/Atikokan] helloile.php we are about to start ...
[17-Feb-2016 16:08:15 America/Atikokan] helloile.php we are about to callback to ILE IleCallMe  ...
[17-Feb-2016 16:08:15 America/Atikokan] helloile.php we are back from ILE IleCallMe.
```


### Contribution guidelines ###
* [Watch this](http://bit.ly/ibmi-git-pull-request) to learn how to do pull requests on IBM i.
* Make sure anything you change works before you put it back (test before git push, Dude!)
* Everything you contribute is under BSD license, aka, you are giving to community.
* All RPG you create should be RPG free form

Quick summary steps to contribute (Aaron video).
```
===
you (not rangercairns)
===
-> fork ... create personal repository (rangercairns)
   https://bitbucket.org/rangercairns/vlang-rpg
-> clone on laptop (or ibm i)
   > git clone https://rangercairns@bitbucket.org/rangercairns/vlang-rpg.git
   ... make changes to source ...
   > git status
   > git add .
   > git commit -m 'short change description'
   > git log
   > git remote -v
   > git push origin master
   ... push to my bitbucket repository (rangercairns) ...
-> create a pull request
   ... now in hands of owners ...
====
Ranger/Aaron
====
-> pull requests
   ... owners ...
   ... review ...
   ... merge ...
```

### RPG free form links (we should all join the future, yes??) ###
* http://www.ibm.com/developerworks/ibmi/library/i-ibmi-rpg-support/

### PHP types ###
* https://github.com/php/php-src/blob/master/Zend/zend_types.h

### Whom do I talk to? ###
* adc@us.ibm.com
