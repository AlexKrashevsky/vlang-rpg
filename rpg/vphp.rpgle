     H NOMAIN
     H AlwNull(*UsrCtl)
     H BNDDIR('QC2LE')

      /copy vlic_h
      /copy vpase_h
      /copy vconv_h
      /copy virt_h
      /copy vphp_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr PHPVirtualCall32 IND;
          iCall POINTER(*PROC) VALUE;
          argc INT(10) VALUE;
          argv POINTER dim(VIRT_CALLARG_MAX) VALUE;
          ileDesc likeds(virtVar_t) dim(VIRT_CALLARG_MAX);
          paseDesc likeds(virtVar_t) dim(VIRT_CALLARG_MAX);
       end-pr;

       // *************************************************
       // PHP API -- start 32bit php
       // *************************************************
       dcl-proc PHPStart32 export;
         dcl-pi *N IND;
          ini CHAR(65500) VALUE options (*nopass);
         end-pi;
         DCL-S rcb IND INZ(*OFF);
         dcl-s pgm CHAR(65500) inz(*BLANKS);
         dcl-s arg CHAR(65500) dim(8) inz(*BLANKS);
         dcl-s env CHAR(65500) dim(8) inz(*BLANKS);

         pgm = PHP_ARG0_PGM;

         arg(1) = PHP_ARG1_CHAT;
         // TBD
         // arg(2) -c
         // arg(3) /path/php.ini

         env(1) = PHP_ENV1_PATH;
         env(2) = PHP_ENV2_LIBPATH;
         env(3) = PHP_ENV3_ATTACH;

         rcb = PaseStart32(pgm:arg:env);

         VirtualSetLanguage(%paddr(PHPVirtualCall32));

         return rcb;
       end-proc; 

       // *************************************************
       // PHP API -- call/pass php script
       // *************************************************
       dcl-proc PHPScript32 export;
         dcl-pi *N IND;
          scriptChunk CHAR(65500) VALUE;
         end-pi;
         DCL-S rcb IND INZ(*OFF);

         rcb = VirtualCallScript32(scriptChunk);

         return rcb;
       end-proc; 

       // *************************************************
       // PHP API -- stop php script
       // *************************************************
       dcl-proc PHPStop export;
         PaseStop();
       end-proc;

       // *************************************************
       // PHP API -- PASE lang callback copy
       // *************************************************
       dcl-proc PHPVirtualCall32 export;
         dcl-pi *N IND;
          iCall POINTER(*PROC) VALUE;
          argc INT(10) VALUE;
          argv POINTER dim(VIRT_CALLARG_MAX) VALUE;
          ileDesc likeds(virtVar_t) dim(VIRT_CALLARG_MAX);
          paseDesc likeds(virtVar_t) dim(VIRT_CALLARG_MAX);
         end-pi;
         dcl-s rcb IND INZ(*ON);
         dcl-pr ClientCallBack IND EXTPROC(iCall);
          arg1 POINTER VALUE;
          arg2 POINTER VALUE;
          arg3 POINTER VALUE;
          arg4 POINTER VALUE;
          arg5 POINTER VALUE;
          arg6 POINTER VALUE;
          arg7 POINTER VALUE;
          arg8 POINTER VALUE;
          arg9 POINTER VALUE;
          arg10 POINTER VALUE;
          arg11 POINTER VALUE;
          arg12 POINTER VALUE;
          arg13 POINTER VALUE;
          arg14 POINTER VALUE;
          arg15 POINTER VALUE;
          arg16 POINTER VALUE;
          arg17 POINTER VALUE;
          arg18 POINTER VALUE;
          arg19 POINTER VALUE;
          arg20 POINTER VALUE;
          arg21 POINTER VALUE;
          arg22 POINTER VALUE;
          arg23 POINTER VALUE;
          arg24 POINTER VALUE;
          arg25 POINTER VALUE;
          arg26 POINTER VALUE;
          arg27 POINTER VALUE;
          arg28 POINTER VALUE;
          arg29 POINTER VALUE;
          arg30 POINTER VALUE;
          arg31 POINTER VALUE;
          arg32 POINTER VALUE;
         end-pr;
         dcl-s rc INT(10) inz(0);
         dcl-s i INT(10) inz(0);
         dcl-s ilev POINTER dim(VIRT_CALLARG_MAX) INZ(*NULL);
         dcl-s ileCCSID INT(10) inz(0);
         dcl-s paseCCSID INT(10) inz(0);

         // TBD set the CCSID
         ileCCSID = 0;
         paseCCSID = PaseLstCCSID();

         // copy in (PASE to ILE)
         for i = 1 to argc;
           rc = convToILE(argv(i):%addr(ilev(i))
                :ileDesc(i):paseDesc(i)
                :ileCCSID:paseCCSID);
         endfor;

         // make call to registered user function
         rcb = ClientCallBack(
          ilev(1):ilev(2):ilev(3):ilev(4):ilev(5):
          ilev(6):ilev(7):ilev(8):ilev(9):ilev(10):
          ilev(11):ilev(12):ilev(13):ilev(14):ilev(15):
          ilev(16):ilev(17):ilev(18):ilev(19):ilev(20):
          ilev(21):ilev(22):ilev(23):ilev(24):ilev(25):
          ilev(26):ilev(27):ilev(28):ilev(29):ilev(30):
          ilev(21):ilev(22));

         // copy out (back to PASE)
         for i = 1 to argc;
           rc = convToPase(%addr(argv(i)):ilev(i)
                :ileDesc(i):paseDesc(i)
                :ileCCSID:paseCCSID);
         endfor;

         return *ON;
       end-proc; 

