      /if defined(VOS_H)
      /eof
      /endif
      /define VOS_H

       // *************************************************
       // misc APIs -- operating system
       // *************************************************
       dcl-pr cpybytes EXTPROC('_CPYBYTES');
         pTarget POINTER VALUE;
         pSource POINTER VALUE;
         nLength UNS(10) VALUE;
       end-pr;

       dcl-pr cpybwp EXTPROC('_CPYBWP');
         pTarget POINTER VALUE;
         pSource POINTER VALUE;
         nLength UNS(10) VALUE;
       end-pr;

       dcl-pr memset POINTER EXTPROC('__memset');
         pTarget POINTER VALUE;
         nChar INT(10) VALUE;
         nBufLen UNS(10) VALUE;
       end-pr;

       dcl-pr memcmp INT(10) EXTPROC('__memcmp');
         pS1 POINTER VALUE;
         pS2 POINTER VALUE;
         nBufLen UNS(10) VALUE;
       end-pr;

