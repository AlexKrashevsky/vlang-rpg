     H AlwNull(*UsrCtl)

      /copy virt_h
      /copy vphp_h

       // *************************************************
       // php script to run
       // *************************************************
       DCL-C PHP_HELLO CONST('include "/usr/local/zendsvr6/share+
       /vlang/php/helloile.php";');

       // *************************************************
       // internal
       // *************************************************
       dcl-pr IleCallMe;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 CHAR(50);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
       end-pr;

       // *************************************************
       // main program
       // *************************************************
       PHPStart32();
       VirtualAddCallBack('IleCallMe':%paddr(IleCallMe));
       VirtualAddCallBackDesc('IleCallMe':VIRT_CHAR:100:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_FLOAT8:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_CHAR:50:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_INT10:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_ZONED:7:4:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_PACKED:12:2:VIRT_IO_BOTH);
       PHPScript32(PHP_HELLO);
       PHPStop();
       return;

       // *************************************************
       // PHP callback location
       // *************************************************
       dcl-proc IleCallMe export;
         dcl-pi *N;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 CHAR(50);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
         end-pi;

         meChar100 = 'hi from ILE RPG free.';
         meFloat8 = 4.1;
         meChar50 = 'VLANG';
         meInt = 1;
         meZoned = 234.5678;
         mePacked = 321.21;

       end-proc; 


